package be.kdg.Service;

import java.util.Date;

public interface RecipeServiceInt {

    public int getRecipeCost(int recipeId, Date date);
}
