package be.kdg.Controller;

import be.kdg.Service.RecipeService;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
public class SaleController {

    RecipeService recipeService;

    public int getRecipeCost(int recipeId, Date date)
    {
       return recipeService.getRecipeCost(recipeId, date);
    }
}
