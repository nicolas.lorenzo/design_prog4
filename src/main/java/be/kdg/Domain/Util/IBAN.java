package be.kdg.Domain.Util;

public class IBAN {
    private String BIC;
    private String country;
    private int accountNumber;
    private int check;

    public IBAN (String BIC, String country, int accountNumber, int check)
    {
        setBIC(BIC);
        setCheck(check);
        setCountry(country);
        setAccountNumber(accountNumber);
    }

    public String getBIC() {
        return BIC;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (country.length() == 2)
        {
            this.country = country;
        }
        else {
            System.out.println("Country needs to be 2 letters");
        }
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        if (check >= 0 && check < 100)
        {
            this.check = check;
        }
        else {
            System.out.println("Number needs to be within 0-99");
        }
    }
}
