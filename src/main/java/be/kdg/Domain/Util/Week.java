package be.kdg.Domain.Util;

public class Week {

    private int year;
    private int weekNumber;

    public Week(int year, int weekNumber)
    {

    }

    public void setWeekNumber(int weekNumber)
    {
        if (weekNumber > 0 && weekNumber <54)
        {
            this.weekNumber = weekNumber;
        }
        else {
            System.out.println("Week needs to be within 1-53");
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
}
