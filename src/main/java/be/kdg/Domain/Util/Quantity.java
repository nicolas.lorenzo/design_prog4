package be.kdg.Domain.Util;

import javax.persistence.*;

@Entity
@Table(name = "Quantity")
public class Quantity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int quantityId;
    private Unit unit;
    private double number;

    public Quantity(Unit unit, double number)
    {
        this.unit = unit;
        this.number = number;
    }

    public Quantity() {

    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }
}
