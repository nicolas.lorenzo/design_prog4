package be.kdg.Domain.Util;

public class BankAccount {

    private String BIC;
    private IBAN iban;

    public BankAccount(String BIC, IBAN iban)
    {

    }

    public String getBIC() {
        return BIC;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }

    public IBAN getIban() {
        return iban;
    }

    public void setIban(IBAN iban) {
        this.iban = iban;
    }
}
