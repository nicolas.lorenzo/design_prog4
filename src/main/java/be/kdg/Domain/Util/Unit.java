package be.kdg.Domain.Util;

public enum Unit {
    GRAM, MILILITRE
}
