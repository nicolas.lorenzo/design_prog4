package be.kdg.Domain.Util;

public class Municipality {
    private String zip;
    private String name;

    public Municipality()
    {

    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
