package be.kdg.Domain.Util;

public class VATNumber {

    private String countryCode;
    private int number;

    public VATNumber(String countryCode, int number)
    {
        this.countryCode = countryCode;
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
