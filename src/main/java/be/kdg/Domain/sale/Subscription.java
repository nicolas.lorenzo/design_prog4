package be.kdg.Domain.sale;

public class Subscription {

    private Interval interval;

    public Subscription() {
    }

    public Interval getInterval() {
        return interval;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }
}
