package be.kdg.Domain.sale;

public class Meal {
    private int nrOfPerson;

    public Meal() {
    }

    public int getNrOfPerson() {
        return nrOfPerson;
    }

    public void setNrOfPerson(int nrOfPerson) {
        if (nrOfPerson <1 || nrOfPerson >6){
            System.out.println("Too less or too many people...");
        }else{
            this.nrOfPerson = nrOfPerson;
        }
    }
}
