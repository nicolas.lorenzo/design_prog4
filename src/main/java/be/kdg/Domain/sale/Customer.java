package be.kdg.Domain.sale;

public class Customer {

    private String name;
    private Address address;
    private BankAccount debitPermission;
    private int nrOfPerson;

    public Customer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public BankAccount getDebitPermission() {
        return debitPermission;
    }

    public void setDebitPermission(BankAccount debitPermission) {
        this.debitPermission = debitPermission;
    }

    public int getNrOfPerson() {
        return nrOfPerson;
    }


        public void setNrOfPerson(int nrOfPerson) {
            if (nrOfPerson <1 || nrOfPerson >6){
                System.out.println("Too less or too many people...");
            }else{
            this.nrOfPerson = nrOfPerson;
        }
    }


}
