package be.kdg.Domain.sale;

import java.util.Date;

public class Transport {

    private String id;
    private Date sentAt;
    private Date receivedAt;

    private enum type{
        SUPPLIER, CUSTOMER;
    }

    public Transport() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getSentAt() {
        return sentAt;
    }

    public void setSentAt(Date sentAt) {
        this.sentAt = sentAt;
    }

    public Date getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(Date receivedAt) {
        this.receivedAt = receivedAt;
    }
}
