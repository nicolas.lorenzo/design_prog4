package be.kdg.Domain.sale;

public class WeekOffer {

    private Week week;
    private double salesPricePerPerson;

    public WeekOffer() {
    }

    public Week getWeek() {
        return week;
    }

    public void setWeek(Week week) {
        this.week = week;
    }

    public double getSalesPricePerPerson() {
        return salesPricePerPerson;
    }

    public void setSalesPricePerPerson(double salesPricePerPerson) {
        this.salesPricePerPerson = salesPricePerPerson;
    }
}
