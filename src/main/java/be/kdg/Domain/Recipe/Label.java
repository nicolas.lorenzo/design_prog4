package be.kdg.Domain.Recipe;

public class Label {
    private String name;

    public Label()
    {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
