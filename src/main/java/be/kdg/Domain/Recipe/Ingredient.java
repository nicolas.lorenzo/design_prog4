package be.kdg.Domain.Recipe;

import be.kdg.Domain.Util.Address;
import be.kdg.Domain.Util.Quantity;
import be.kdg.Domain.purchase.Product;

import javax.persistence.*;

@Entity
@Table(name = "Ingredient")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ingredientId;

    @OneToOne
    @JoinColumn(name = "quantity_id")
    private Quantity quantity;

    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="product_id")
    private Product product;

    public Ingredient()
    {

    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }
}
