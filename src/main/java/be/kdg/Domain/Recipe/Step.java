package be.kdg.Domain.Recipe;

public class Step {
    private String description;

    public Step(){

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
