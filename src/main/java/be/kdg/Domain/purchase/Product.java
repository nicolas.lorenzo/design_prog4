package be.kdg.Domain.purchase;

import be.kdg.Domain.Recipe.Ingredient;
import be.kdg.Domain.Util.Unit;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productId;

    @Column(nullable = false)
    private String name;
    @Enumerated(EnumType.STRING)
    private Unit unit;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Contract> contracts;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Ingredient> ingredients;

    public Product() {
    }

    public Product(int productId,String name, Unit unit) {
        this.name = name;
        this.unit = unit;
        this.productId = productId;
        contracts = new ArrayList<>();
        ingredients = new ArrayList<>();
    }

    public Product(String name, Unit unit) {
        this.name = name;
        this.unit = unit;
        contracts = new ArrayList<>();
        ingredients = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addContract(Contract contract){
        contracts.add(contract);
    }

    public void deleteContract(Contract contract){
        contracts.remove(contract);
    }

    public void addIngredient(Ingredient ingredient){
        ingredients.add(ingredient);
    }

    public void deleteIngredient(Ingredient ingredient){
        ingredients.remove(ingredient);
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }
}
