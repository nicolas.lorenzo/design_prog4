package be.kdg.Domain.purchase;

import be.kdg.Domain.Util.Interval;
import be.kdg.Domain.Util.Quantity;

import javax.persistence.*;

@Entity
@Table(name="Clause")
public class Clause {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clauseId;
    private double price;

    @OneToOne
    @JoinColumn(name = "clause_id")
    private Interval period;
    @OneToOne
    @JoinColumn(name = "clause_id")
    private Quantity maxQuantity;
    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="student_house_id")
    private Contract contract;

    public Clause(){

    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Interval getPeriod() {
        return period;
    }

    public void setPeriod(Interval period) {
        this.period = period;
    }

    public Quantity getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Quantity maxQuantity) {
        this.maxQuantity = maxQuantity;
    }
}
