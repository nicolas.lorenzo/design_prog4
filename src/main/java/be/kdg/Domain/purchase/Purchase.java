package be.kdg.Domain.purchase;

import be.kdg.Domain.Util.Quantity;

import java.util.Date;

public class Purchase {

    private Quantity quantity;
    private Date date;
    private Date cofirmationDate;
    private Boolean withContract;

    private enum state{
        ORDER, CANCELED, CONFIRMED, DELIVERED;
    }

    public Purchase() {
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCofirmationDate() {
        return cofirmationDate;
    }

    public void setCofirmationDate(Date cofirmationDate) {
        this.cofirmationDate = cofirmationDate;
    }

    public Boolean getWithContract() {
        return withContract;
    }

    public void setWithContract(Boolean withContract) {
        this.withContract = withContract;
    }
}
