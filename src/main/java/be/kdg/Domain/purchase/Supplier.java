package be.kdg.Domain.purchase;

import be.kdg.Domain.Util.VATNumber;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Supplier")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private int supplierId;
    private String name;
    private VATNumber VAT;
    private int reputation;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<Contract> contracts;

    public Supplier() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VATNumber getVAT() {
        return VAT;
    }

    public void setVAT(VATNumber VAT) {
        this.VAT = VAT;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }
}
