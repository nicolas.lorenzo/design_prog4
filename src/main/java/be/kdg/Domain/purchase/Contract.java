package be.kdg.Domain.purchase;

import be.kdg.Domain.Util.Unit;
import jdk.jfr.Enabled;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Contract")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int contractId
            ;

    @Column(nullable = false)
    private long nr;

    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="supplier_id")
    private DistributionCentre distributionCentre;
    @Column(nullable = false)
    private Date signDate;
    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="product_id")
    private Product product;

    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="supplier_id")
    private Supplier supplier;
    @OneToMany(mappedBy = "contract", cascade = CascadeType.ALL)
    private List<Clause> clauses;
    public Contract(){

    }

    public Contract(long nr, Date signDate, Product product, Supplier supplier, DistributionCentre distributionCentre) {
        this.nr = nr;
        this.signDate = signDate;
        this.product = product;
        this.supplier = supplier;
        this.distributionCentre = distributionCentre;
        clauses = new ArrayList<>();
    }

    public Contract(int contractId, long nr, Date signDate, Product product, Supplier supplier) {
        this.contractId = contractId;
        this.nr = nr;
        this.signDate = signDate;
        this.product = product;
        this.supplier = supplier;
        this.distributionCentre = distributionCentre;
        clauses = new ArrayList<>();
    }

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }

    public long getNr() {
        return nr;
    }

    public void setNr(long nr) {
        this.nr = nr;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

    public void addClause(Clause clause){
        clauses.add(clause);
    }

    public void deleteClause(Clause clause){
        clauses.remove(clause);
    }
}
