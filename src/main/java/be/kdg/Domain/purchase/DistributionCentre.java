package be.kdg.Domain.purchase;

import be.kdg.Domain.Util.Address;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="DistributionCentre")
public class DistributionCentre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int distributionCentreId;


    @OneToMany(mappedBy = "distributionCentre", cascade = CascadeType.ALL)
    private List<Contract> contracts;
    private Address address;

    public DistributionCentre() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
